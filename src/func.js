const {listOfPosts} = require("./posts");
const getSum = (str1, str2) => {
  // add your implementation below
  if(str1 === "" || str2 === ""){
    if(str1 === ""){
      return str2;
    }
    if(str2 === ""){
      return str1;
    }
  }
  if(typeof str1 !== "string" || typeof str2 !== "string" || isNaN(parseInt(str1)) || isNaN(parseInt(str2)))
    return false;
  else{
    let char1, char2, resultChar = [];
    char1 = str1.split("");
    char2 = str2.split("");
    for (let i = 0; i < char1.length; i++){
      resultChar.push(parseInt(char1[i])+parseInt(char2[i]));
    }
    return resultChar.join("");
  }


};
let listOfPosts2 = [
  {
    id: 1,
    post: 'some post1',
    title: 'title 1',
    author: 'Ivanov',
    comments: [
      {
        id: 1.1,
        comment: 'some comment1',
        title: 'title 1',
        author: 'Rimus'
      },
      {
        id: 1.2,
        comment: 'some comment2',
        title: 'title 2',
        author: 'Uncle'
      }
    ]
  },
  {
    id: 2,
    post: 'some post2',
    title: 'title 2',
    author: 'Ivanov',
    comments: [
      {
        id: 1.1,
        comment: 'some comment1',
        title: 'title 1',
        author: 'Rimus'
      },
      {
        id: 1.2,
        comment: 'some comment2',
        title: 'title 2',
        author: 'Uncle'
      },
      {
        id: 1.3,
        comment: 'some comment3',
        title: 'title 3',
        author: 'Rimus'
      }
    ]
  },
  {
    id: 3,
    post: 'some post3',
    title: 'title 3',
    author: 'Rimus'
  },
  {
    id: 4,
    post: 'some post4',
    title: 'title 4',
    author: 'Uncle'
  }

]

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let countPosts = 0;
  let countComments = 0;
  for (const listOfPost of listOfPosts) {
    if(listOfPost.author === authorName) {
      countPosts++;
    }
    if(listOfPost.comments) {
      for (let i = 0; i < listOfPost.comments.length; i++) {
        if (listOfPost.comments[i].author === authorName) {
          countComments++;
        }
      }
    }
    }
  return `Post:${countPosts},comments:${countComments}`;
};
const tickets=(people)=> {
  // add your implementation below
  let arrayOfMoney = [];
  let checkOn25 = false;
  let checkOn50 = false;
  for(let countOfMoney of people){
    if(countOfMoney === 25){
      arrayOfMoney.push(countOfMoney);
    }
    if(countOfMoney === 50 && arrayOfMoney.length > 0){
      for(let i = 0; i < arrayOfMoney.length; i++){
        if(arrayOfMoney[i] === 25){
          arrayOfMoney.splice(i, 1);
          arrayOfMoney.push(countOfMoney);
          break;
        }
        if(i === arrayOfMoney.length - 1){
         return "NO";
        }
      }
    }
    if(countOfMoney == 100){
      if(arrayOfMoney.length < 2){
        return "NO";
      }
      for(let i = 0; i < arrayOfMoney.length; i++){
        if(checkOn25 && checkOn50){
          break;
        }
        if(arrayOfMoney[i] === 25 && checkOn25 !== true){
          arrayOfMoney.splice(i, 1);
          arrayOfMoney.push(countOfMoney);
          checkOn25 = true;
        }
        if(arrayOfMoney[i] === 50 && checkOn50 !== true){
          arrayOfMoney.splice(i, 1);
          arrayOfMoney.push(countOfMoney);
          checkOn50 = true;
        }
        if(i == arrayOfMoney.length - 1 && (checkOn25 !== true || checkOn50 !== true)){
          return "NO";
        }
      }
    }
  }
  return "YES";
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
